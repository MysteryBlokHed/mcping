mcping
======

Get statuses from Minecraft servers.

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   mcping

* :ref:`genindex`

Use
---

Synchronous:

.. code-block:: python

   import mcping
   # Providing only IP
   status = mcping.status('127.0.0.1')
   # Providing IP and port
   status = mcping.status('127.0.0.1', 25565)
   # Providing IP and timeout
   status = mcping.status('127.0.0.1', timeout=5.0)
   # Providing IP, port, and timeout
   status = mcping.status('127.0.0.1', 25565, 5.0)

   # Do not check SRV record (avoids sometimes-unneeded DNS query)
   status = mcping.status('127.0.0.1', timeout=5.0, srv_check=False)

Asynchronous:

.. code-block:: python

   import mcping
   # Providing only IP
   status = await mcping.async_status('127.0.0.1')
   # Providing IP and port
   status = await mcping.async_status('127.0.0.1', 25565)
   # Providing IP and timeout
   status = await mcping.async_status('127.0.0.1', timeout=5.0)
   # Providing IP, port, and timeout
   status = await mcping.async_status('127.0.0.1', 25565, 5.0)

   # Do not check SRV record (avoids sometimes-unneeded DNS query)
   status = await mcping.async_status('127.0.0.1', timeout=5.0, srv_check=False)

Indices and tables
==================

* :ref:`genindex`
