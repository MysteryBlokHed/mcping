import os
import sys

sys.path.insert(0, os.path.abspath(".."))

project = "mcping"
copyright = "2022, Adam Thompson-Sharpe"
author = "Adam Thompson-Sharpe"
release = "0.3.0"

# General configuration

extensions = ["sphinx.ext.napoleon", "sphinx.ext.autodoc"]
templates_path = ["_templates"]
exclude_patterns = ["_build", "Thumbs.db", ".DS_Store"]

# Options for HTML output

html_theme = "sphinx_rtd_theme"
html_static_path = ["_static"]
